<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PhpDiBootstrapper;

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RecursiveRegexIterator;
use RegexIterator;
use Webmozart\Assert\Assert;

/**
 * Bootstrapping helper for PHP-DI
 */
class Bootstrap
{

    /**
     * @var string|null
     */
    private ?string $compilationDirectory = null;

    /**
     * @var ContainerBuilder|null
     */
    private ?ContainerBuilder $builder;

    /**
     * @var DefinitionSource[]
     */
    private array $definitionSources = [];

    /**
     * @var bool
     */
    private bool $forceRecompile = false;

    /**
     * Creates a new builder
     *
     * @param \DI\ContainerBuilder|null $builder
     */
    public function __construct(?ContainerBuilder $builder = null)
    {
        if ($builder === null) {
            $builder = new ContainerBuilder();
        }

        $this->builder = $builder;
    }

    /**
     * Add a directory to read definitions from. Definitions are always loaded
     * alphabetically.
     *
     * @param string $directory
     * @param bool   $recursive
     */
    public function addDefinitionsFromDirectory(string $directory, bool $recursive = true): void
    {
        $this->definitionSources[] = new DirectorySource($directory, $recursive);
    }

    /**
     * Add an array to read definitions from
     *
     * @param array $definition
     */
    public function addDefinitionArray(array $definition): void
    {
        $this->definitionSources[] = new ArrayDefinition($definition);
    }

    public function forceRecompile(bool $force): void
    {
        $this->forceRecompile = $force;
    }

    /**
     * Enable compilation, and compile to the specified directory
     *
     * @param string $directory
     */
    public function enableCompilation(string $directory): void
    {
        $this->compilationDirectory = $directory;
    }

    /**
     * @return \Psr\Container\ContainerInterface
     * @throws \Exception
     */
    public function create(): ContainerInterface
    {
        $builder = $this->getBuilder();

        if ($this->compilationDirectory !== null) {
            if ($this->forceRecompile) {
                $this->deleteCompiledContainer($this->compilationDirectory);
            }

            $this->mkdirr($this->compilationDirectory);
            $builder->enableCompilation($this->compilationDirectory);
        }

        if ($this->needsCompilation()) {
            foreach ($this->getDefinitions() as $definition) {
                $builder->addDefinitions($definition);
            }
        }

        $container = $builder->build();
        $this->builder = null;

        return $container;
    }

    /**
     * Checks if the builder is ready
     *
     * @internal
     * @psalm-assert \DI\ContainerBuilder $builder
     */
    private function validateBuilder(?ContainerBuilder $builder): void
    {
        if ($builder === null) {
            throw new \LogicException("Builder is not set");
        }
    }

    /**
     * Check if the builder needs compilation
     */
    private function needsCompilation(): bool
    {
        if ($this->compilationDirectory === null) {
            return true;
        }

        if (!$this->compiledContainerExists()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function compiledContainerExists(): bool
    {
        if ($this->compilationDirectory === null) {
            return false;
        }
        return file_exists(implode(DIRECTORY_SEPARATOR, [$this->compilationDirectory, "CompiledContainer.php"]));
    }

    /**
     * @return \Generator
     * @psalm-return \Generator<string|array|\DI\Definition\Source\DefinitionSource>
     */
    private function getDefinitions(): \Generator
    {
        foreach ($this->definitionSources as $source) {
            yield from $source->getDefinitions();
        }
    }

    private function getBuilder(): ContainerBuilder
    {
        $builder = $this->builder;
        $this->validateBuilder($builder);
        return $builder;
    }

    /**
     * @param string $dir
     */
    private function mkdirr(string $dir): void
    {
        if (!file_exists($dir)) {
            $parent = dirname($dir);
            if (($parent != $dir) && (!file_exists($dir))) {
                $this->mkdirr($parent);
            }
            mkdir($dir);
        }
    }

    /**
     * @param string $compilationDirectory
     */
    private function deleteCompiledContainer(string $compilationDirectory): void
    {
        $file = implode(DIRECTORY_SEPARATOR, [$this->compilationDirectory, "CompiledContainer.php"]);

        if (file_exists($file)) {
            unlink($file);
        }
    }
}
