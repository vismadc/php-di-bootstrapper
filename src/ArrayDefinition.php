<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PhpDiBootstrapper;

/**
 * @internal
 */
class ArrayDefinition implements DefinitionSource
{
    /**
     * @var array
     */
    private array $definition;

    /**
     * ArrayDefinition constructor.
     *
     * @param array $definition
     */
    public function __construct(array $definition)
    {
        $this->definition = $definition;
    }

    /**
     * @inheritDoc
     * @psalm-return \Generator<string|array|\DI\Definition\Source\DefinitionSource>
     */
    public function getDefinitions(): iterable
    {
        yield $this->definition;
    }
}
