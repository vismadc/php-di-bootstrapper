<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PhpDiBootstrapper;

use LogicException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

function rrmdir(string $dir): void
{
    if (!file_exists($dir)) {
        return;
    }
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );

    /** @var mixed $fileinfo */
    foreach ($files as $fileinfo) {
        if (!$fileinfo instanceof SplFileInfo) {
            throw new LogicException("Invalid object returned from iterator");
        }
        if ($fileinfo->isDir()) {
            rmdir($fileinfo->getRealPath());
        } else {
            unlink($fileinfo->getRealPath());
        }
    }

    rmdir($dir);
}
