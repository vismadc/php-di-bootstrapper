<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PhpDiBootstrapper;

use Webmozart\Assert\Assert;

class DirectorySource implements DefinitionSource
{
    /**
     * @var string
     */
    protected string $directory;
    /**
     * @var bool
     */
    protected bool $recursive;

    public function __construct(string $directory, bool $recursive)
    {
        $this->directory = $directory;
        $this->recursive = $recursive;
    }

    /**
     * @inheritDoc
     * @psalm-return \Generator<string|array|\DI\Definition\Source\DefinitionSource>
     */
    public function getDefinitions(): \Generator
    {
        $files = $this->getPhpFiles($this->directory, $this->recursive);
        $definitions = iterator_to_array($files);
        natcasesort($definitions);

        foreach ($definitions as $file) {
            Assert::file($file);
            /**
             * @var            mixed $data
             * @psalm-suppress UnresolvableInclude
             */
            $data = include $file;
            if (!(is_array($data) || ($data instanceof \DI\Definition\Source\DefinitionSource) || is_string($data))) {
                throw new \LogicException(sprintf("Invalid definition in file %s", $file));
            }
            yield $data;
        }
    }

    /**
     * @param string $directory
     * @param bool   $recursive
     * @return       \Generator|string[]
     * @psalm-return \Generator<string>
     *
     * @see https://www.php.net/manual/en/class.recursivedirectoryiterator.php#97228
     */
    private function getPhpFiles(string $directory, bool $recursive): \Generator
    {
        if ($recursive) {
            $dir = new \RecursiveDirectoryIterator($directory);
            $iter = new \RecursiveIteratorIterator($dir);
            $files = new \RegexIterator($iter, '/^.+\.php$/i', \RegexIterator::GET_MATCH);
        } else {
            $dir = new \DirectoryIterator($directory);
            $iter = (function (\DirectoryIterator $dir): \Generator {
                /** @var \DirectoryIterator $file */
                foreach ($dir as $file) {
                    yield $file->getPathname();
                }
            })($dir);
            $files = new \RegexIterator($iter, '/^.+\.php$/i', \RegexIterator::GET_MATCH);
        }

        /**
         * @var mixed $file
         */
        foreach ($files as $file) {
            Assert::isArray($file);
            Assert::count($file, 1);
            Assert::keyExists($file, 0);
            Assert::string($file[0]);
            yield $file[0];
        }
    }
}
