<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PhpDiBootstrapper;

/**
 * @internal
 */
interface DefinitionSource
{
    /**
     * @return iterable
     * @psalm-return iterable<string|array|\DI\Definition\Source\DefinitionSource>
     */
    public function getDefinitions(): iterable;
}
