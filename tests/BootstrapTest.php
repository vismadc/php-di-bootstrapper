<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PhpDiBootstrapper\Test;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Visma\PhpDiBootstrapper\Bootstrap;

use function DI\factory;
use function DI\get;
use function Visma\PhpDiBootstrapper\rrmdir;

/**
 * @covers \Visma\PhpDiBootstrapper\Bootstrap
 * @uses \Visma\PhpDiBootstrapper\rrmdir
 */
class BootstrapTest extends TestCase
{
    /**
     * @var string
     */
    private $tempDir;

    protected function setUp(): void
    {
        $tempDir = sys_get_temp_dir();
        $base = implode(
            DIRECTORY_SEPARATOR,
            [$tempDir, sprintf("container-test-%s", bin2hex(random_bytes(4)))]
        );
        $this->tempDir = $base;
        if (!file_exists($base)) {
            mkdir($base);
        }
    }

    /**
     */
    protected function tearDown(): void
    {
        rrmdir($this->tempDir);
    }

    public function testCanBuild()
    {
        $bs = new Bootstrap();
        $c = $bs->create();

        $this->assertInstanceOf(ContainerInterface::class, $c);
    }

    /**
     * @throws \Exception
     * @uses \Visma\PhpDiBootstrapper\ArrayDefinition
     */
    public function testCanBuildWithArrayDefinition()
    {
        $bs = new Bootstrap();
        $bs->addDefinitionArray(
            [
                "sample" => 0.25,
                "factory" => factory(
                    function (float $sample): float {
                        return $sample * 2;
                    }
                )
                    ->parameter("sample", get("sample")),
            ]
        );
        $c = $bs->create();

        $this->assertInstanceOf(ContainerInterface::class, $c);
        $this->assertEquals(0.5, $c->get("factory"));
    }

    public function testItBuildsDirectory()
    {
        $containerDir = implode(DIRECTORY_SEPARATOR, [$this->tempDir, "test", "container", "dir"]);

        $bs = new Bootstrap();
        $bs->enableCompilation($containerDir);

        $bs->create();

        $this->assertFileExists(implode(DIRECTORY_SEPARATOR, [$containerDir, "CompiledContainer.php"]));
    }
}
