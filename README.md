# Bootstrap helpers for [PHP-DI]

## Usage
Make an empty container (not very useful):

```php
$bootstrap = new \Visma\PhpDiBootstrapper\Bootstrap();

$container = $bootstrap->create();
```

## Usage with files
Load all files in the provided directory recusively as definitions, sorted by 
name

```php
$bootstrap = new \Visma\PhpDiBootstrapper\Bootstrap();
$bootstrap->addDefinitionsFromDirectory(__DIR__ . '/src/definitions/');

$container = $bootstrap->create();
```

## Usage with files and container compilation
Load all files in the provided directory recusively as definitions, sorted by 
name, only reading the files if the container is not already compiled

```php
$bootstrap = new \Visma\PhpDiBootstrapper\Bootstrap();
$bootstrap->enableCompilation(__DIR__ . '/generated/container/');
$bootstrap->addDefinitionsFromDirectory(__DIR__ . '/src/definitions/');

$container = $bootstrap->create();
```

[PHP-DI]: http://php-di.org/
